// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseEnemy.h"
#include "GlobalDefines.h"
#include "HealthComponent.h"
#include "NavigationPath.h"
// Sets default values
ABaseEnemy::ABaseEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    Type = ETargetType::tBuildings;
    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();
	HealthComponent->OnDeath.AddUObject(this, &ABaseEnemy::OnDeath);

 //   if (Type == ETargetType::tBuildings)
    {
		SpawnManager::Get().ObjectSpawned(this);
    }
//    else
    {
        //SetTargetASPlayerController?
//        GetPlayer
    }
}

// Called every frame
void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float ABaseEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    return HealthComponent->TakeDamage(Damage, DamageCauser);
}

/*bool ABaseEnemy::IsAbleToAttack()
{
    return true;
}*/


void ABaseEnemy::Attack()
{
	OnAttack();
}


FVector ABaseEnemy::GetTargetLocation(AActor* RequestedBy) const
{	
	return Target ? Target->GetActorLocation() : FVector::ZeroVector;
}

void ABaseEnemy::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	if (HealthComponent->GetCurrentHealth() > 0)
	{
		HealthComponent->OnDeath.Broadcast(this);
	}
}

void ABaseEnemy::OnDeath(AActor*)
{
	OnDeath();
}

void ABaseEnemy::OnDamaged(float, AActor*, AActor*)
{
    
}
