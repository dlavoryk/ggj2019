// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GGJ2019GameMode.generated.h"

UCLASS(minimalapi)
class AGGJ2019GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(Exec) void SpawnBot(int Count = 1);

	AGGJ2019GameMode();
};