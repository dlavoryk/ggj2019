// Fill out your copyright notice in the Description page of Project Settings.

#include "Tower.h"

#include "GlobalDefines.h"
#include "HealthComponent.h"

#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "AIGod.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/Character.h"
#include "Engine/World.h"
// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

}

float ATower::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
    return HealthComponent->TakeDamage(Damage, DamageCauser);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
    CurTime = MaxTime;
	SpawnManager::Get().ObjectSpawned(this);

}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    CurTime -= DeltaTime;
    Attack();
}


bool ATower::IsAbleToAttack()
{
   return CurTime < 0.f;
}

void ATower::Attack()
{
/*    if (IsAbleToAttack())
    {
        FCollisionQueryParams FCQP = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
        FCQP.bTraceComplex = true;
        FCQP.bTraceAsyncScene = false;
        FCQP.bReturnPhysicalMaterial = false;

		// @todo dlavoryk: Add GlobalAccess
        for (auto & Ignored : AAIGod::GetTowers())
        {
            FCQP.AddIgnoredActor(Ignored);
        }
        const AActor * Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
        FCQP.AddIgnoredActor(Player);
        
        
        FCollisionResponseParams FCRP = FCollisionResponseParams(ECR_Overlap);
        
        FVector Start = GetActorLocation();
        FVector End = Start;
        Start.Y += DamageRadius;
        End.Y -= DamageRadius;
        TArray<FHitResult> Hits;
        
        
        OnAttack();
        
        
        if (GetWorld()->SweepMultiByChannel(Hits, Start, End, FQuat(), ECC_Pawn, FCollisionShape::MakeSphere(DamageRadius), FCQP, FCRP))
        {
            for (auto & HitResult : Hits)
            {
                if (HitResult.GetActor())
                {
                    HitResult.GetActor()->TakeDamage(DamageAmount, FDamageEvent(), nullptr, this);
                }
            }
            
        }
        CurTime = MaxTime;
        //
    }
	*/
}


void ATower::OnDeath(AActor*)
{
    
}

void ATower::OnDamaged(float, AActor*, AActor*)
{
    
}
