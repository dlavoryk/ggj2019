// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "MeleeEnemy.generated.h"

/**
 * 
 */
UCLASS()
class GGJ2019_API AMeleeEnemy : public ABaseEnemy
{
	GENERATED_BODY()
	
    
    
public:
    
    virtual bool IsAbleToAttack() override;
    
    virtual void Attack() override;
    
    virtual void Tick(float DeltaTime) override;
protected:
    UPROPERTY(EditAnyWhere) float Range;
    UPROPERTY(EditAnyWhere) float DamageAmount;
    
};
