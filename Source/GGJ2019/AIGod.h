// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NavigationModifier.h"
#include "AIGod.generated.h"

class AAIController;
struct FPathFindingQuery;
struct FAIMoveRequest;
class UNavigationPath;
class ABaseEnemy;
class ATower;


UCLASS()
class GGJ2019_API AAIGod : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAIGod();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	
	FVector StickToTheCener(ABaseEnemy * CurEnemy, const TArray<ABaseEnemy*> & Group) const;
	FVector ImitateCollision(ABaseEnemy * CurEnemy) const;
	float StickToAvgVelocity(ABaseEnemy * CurEnemy) const;
	FVector GetTargetLocation(AActor* RequestedBy) const override;

	void MoveAllBoids(float DeltaTime);

	FVector GetDestinationForBoid(ABaseEnemy * Enemy, float DeltaTime, const TArray<ABaseEnemy*> & Group);

	void FindPathForMoveRequest(const FAIMoveRequest& MoveRequest, FPathFindingQuery& Query, FNavPathSharedPtr& OutPath) const;
	bool BuildPathfindingQuery(const FAIMoveRequest& MoveRequest, FPathFindingQuery& Query, AAIController * Controller) const;


	void Test(const FAIMoveRequest& MoveRequest);

public:


	void SpawnBot();


	void DivideIntoGroups();
	void UpdateTargetForEnemies(AActor * PreviousTarget);

    void RecalculateAllTargets();
    
	// Called every frame
	virtual void Tick(float DeltaTime) override;
    
    void OnEnemiesDeath(AActor * DeadActor);
    void OnTargetDeath(AActor * DeadActor);
    
    void OnObjectSpawned(AActor * SpawnedObject);
    
    void FindTargetFor (ABaseEnemy * Object);
    
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
   
    UPROPERTY(EditAnyWhere) TSubclassOf<ABaseEnemy> EnemyClass;   
    UPROPERTY(EditAnyWhere) float DeathTime = 2.f;

	UPROPERTY(EditAnyWhere, Category = "Boids") float VelocityCoef = 0.125f;
	UPROPERTY(EditAnyWhere, Category = "Boids") float StickToTheCenterCoef = 0.125f;
	UPROPERTY(EditAnyWhere, Category = "Boids") float CollisionCoef = 0.75f;
	UPROPERTY(EditAnyWhere, Category = "Boids") float CollisionR = 400.f;
	UPROPERTY(EditAnyWhere, Category = "Boids") float TargetCoef = 1.f;
	UPROPERTY(EditAnyWhere, Category = "Boids") float RotationSpeed = PI / 4;
	UPROPERTY(EditAnyWhere, Category = "Boids") float NULL_ROTATION = 0.08726f;
	UPROPERTY(EditAnyWhere, Category = "Boids") float DistanceToLiveGroup = 1000.f;

	UPROPERTY(EditAnyWhere, Category = "Pathfinder") float AcceptableR = 10.f;
	UPROPERTY(EditAnyWhere, Category = "Pathfinder") bool StopOnOverlap = true;
	UPROPERTY(EditAnyWhere, Category = "Pathfinder") bool UsePathFinding = true;
	UPROPERTY(EditAnyWhere, Category = "Pathfinder") bool ProjectDestinationToNavigation = false;
	UPROPERTY(EditAnyWhere, Category = "Pathfinder") bool CanStrafe = true;

    TArray<ABaseEnemy*> Enemies;

    TMap<AActor*, TArray<ABaseEnemy*>> EnemiesGroups;

    TArray<ATower*> Targets;
    
	const TArray<ATower*> & GetTowers() const { return Targets; }
	const TArray<ABaseEnemy*> & GetEnemies() const { return Enemies; }
	int mCurIdx = 0;
};
