// Fill out your copyright notice in the Description page of Project Settings.

#include "AIGod.h"

#include "BaseEnemy.h"

#include "Buildings/Tower.h"

#include "HealthComponent.h"

#include "GlobalDefines.h"
#include "Engine/World.h"
#include "AIController.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "NavigationSystem.h"
#include "NavigationPath.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/PlayerController.h"

//TArray<ABaseEnemy*> AAIGod::Enemies;
//
//TArray<ATower*> AAIGod::Targets;

PRAGMA_DISABLE_OPTIMIZATION_ACTUAL

// Sets default values
AAIGod::AAIGod()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AAIGod::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnManager::Get().OnSpawned.RemoveAll(this);
	SpawnManager::Get().OnSpawned.AddUObject(this, &AAIGod::OnObjectSpawned);
	
    for (int i = 0; i < 10; ++i)
    {
		SpawnBot();
        //auto A = 
	//	OnObjectSpawned(A);
    }
    
}

FVector AAIGod::StickToTheCener(ABaseEnemy * CurEnemy, const TArray<ABaseEnemy*> & Group) const
{
	if (Group.Num() < 2) return FVector::ZeroVector;
	FVector Center(ForceInitToZero);
	for (auto & Enemy : Group)
	{
		Center += Enemy->GetActorLocation();
	}
	Center -= CurEnemy->GetActorLocation();
	Center /= (Group.Num() - 1);
	return (Center - CurEnemy->GetActorLocation()).GetSafeNormal() * StickToTheCenterCoef;
}

FVector AAIGod::ImitateCollision(ABaseEnemy* CurEnemy) const
{
	if (Enemies.Num() < 2) return FVector::ZeroVector;
	FVector C(ForceInitToZero);
	const FVector CurActorLocation = CurEnemy->GetActorLocation();
	for (auto & Enemy : Enemies)
	{
		if (CurEnemy != Enemy)
		{
			const FVector Pos = Enemy->GetActorLocation();
			const FVector VecBetweenActors = CurActorLocation - Pos;
			const float Dist = VecBetweenActors.Size();
			if (Dist < CollisionR)
			{
				C += (VecBetweenActors).GetSafeNormal() * (CollisionR - Dist);
			}
		}
	}
	return C.GetSafeNormal() * CollisionCoef;
}

float AAIGod::StickToAvgVelocity(ABaseEnemy* CurEnemy) const
{
	if (Enemies.Num() < 2) return 0;
	float dV = 0.f;
	for (auto & Enemy : Enemies)
	{
		dV += Enemy->GetEnemyVelocity();
	}
	dV -= CurEnemy->GetEnemyVelocity();
	dV /= (Enemies.Num() - 1);
	return (dV - CurEnemy->GetEnemyVelocity()) * VelocityCoef;
}

FVector AAIGod::GetTargetLocation(AActor* RequestedBy) const
{
	const FVector Loc = RequestedBy->GetTargetLocation() - RequestedBy->GetActorLocation();
	return Loc.GetSafeNormal() * TargetCoef;
}

void AAIGod::MoveAllBoids(float DeltaTime)
{
	for (auto & EnemyGroup : EnemiesGroups)
	{
		for (auto & Enemy : EnemyGroup.Value)
		{
			AAIController * Controller = Cast<AAIController>(Enemy->GetController());
			if (Controller && !Enemy->IsAbleToAttack())// && !Controller->IsFollowingAPath())
			{
				const FVector Destination = GetDestinationForBoid(Enemy, DeltaTime, EnemyGroup.Value);
				Controller->MoveToLocation(Destination
					, AcceptableR, StopOnOverlap, UsePathFinding, ProjectDestinationToNavigation, CanStrafe);
				DrawDebugSphere(GetWorld(), Destination + Destination.GetSafeNormal() * 100, 30.0f, 12, FColor(255, 0, 0));
			}
		}
	}

	//for (auto & Enemy : Enemies)
	//{
	//	AAIController * Controller = Cast<AAIController>(Enemy->GetController());
	//	if (Controller && !Enemy->IsAbleToAttack())// && !Controller->IsFollowingAPath())
	//	{
	//		const FVector Destination = GetDestinationForBoid(Enemy, DeltaTime);
	//		Controller->MoveToLocation(Destination
	//			, AcceptableR, StopOnOverlap, UsePathFinding, ProjectDestinationToNavigation, CanStrafe);
	//		DrawDebugSphere(GetWorld(), Destination + Destination.GetSafeNormal() * 100, 30.0f, 12, FColor(255, 0, 0));
	//	}
	//}
}

FVector AAIGod::GetDestinationForBoid(ABaseEnemy* Enemy, float DeltaTime, const TArray<ABaseEnemy*> & Group)
{
//	const float DistanceToEnemy = (Enemy->GetTargetLocation() - Enemy->GetActorLocation()).Size();

	const FVector DirectionToCenter = StickToTheCener(Enemy, Group);
	const FVector DirectionToAvoidCollision = ImitateCollision(Enemy);
	const FVector DirectionToTarget = GetTargetLocation(Enemy);
	FVector ResultDirection = (DirectionToCenter + DirectionToAvoidCollision + DirectionToTarget).GetSafeNormal();
	const FVector CurRotation = Enemy->GetActorRotation().Vector();
	const float DesiredRotation = (FMath::Acos(FVector::DotProduct(ResultDirection, CurRotation) / (ResultDirection - CurRotation).Size()));

	if (!FMath::IsNearlyZero(DesiredRotation, NULL_ROTATION))
	{
		if (DesiredRotation > 0)
		{
			ResultDirection = FMath::Lerp(CurRotation, ResultDirection, RotationSpeed / DesiredRotation * DeltaTime);
		}
		else
		{
			ResultDirection = FMath::Lerp(ResultDirection, CurRotation, -RotationSpeed / DesiredRotation * DeltaTime);
		}
	}
	Enemy->Velocity += StickToAvgVelocity(Enemy);
	const FVector EnemyLocation = Enemy->GetActorLocation();
	const FVector Destination = EnemyLocation + ResultDirection * Enemy->GetEnemyVelocity();// *DeltaTime;
	return Destination;
}

void AAIGod::FindPathForMoveRequest(const FAIMoveRequest& MoveRequest, FPathFindingQuery& Query,
	FNavPathSharedPtr& OutPath) const
{
	UNavigationSystemV1* NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
	if (NavSys)
	{
		FPathFindingResult PathResult = NavSys->FindPathSync(Query);
		if (PathResult.Result != ENavigationQueryResult::Error)
		{
			if (PathResult.IsSuccessful() && PathResult.Path.IsValid())
			{
				if (MoveRequest.IsMoveToActorRequest())
				{
					PathResult.Path->SetGoalActorObservation(*MoveRequest.GetGoalActor(), 100.0f);
				}

				PathResult.Path->EnableRecalculationOnInvalidation(true);
				OutPath = PathResult.Path;
			}
		}
	}
}

bool AAIGod::BuildPathfindingQuery(const FAIMoveRequest& MoveRequest, FPathFindingQuery& Query, AAIController * Controller) const
{
	bool bResult = false;

	UNavigationSystemV1* NavSys = FNavigationSystem::GetCurrent<UNavigationSystemV1>(GetWorld());
	const ANavigationData* NavData = (NavSys == nullptr) ? nullptr :
		MoveRequest.IsUsingPathfinding() ? NavSys->GetNavDataForProps(Controller->GetNavAgentPropertiesRef()) :
		NavSys->GetAbstractNavData();

	if (NavData)
	{
		FVector GoalLocation = MoveRequest.GetGoalLocation();
		if (MoveRequest.IsMoveToActorRequest())
		{
			const INavAgentInterface* NavGoal = Cast<const INavAgentInterface>(MoveRequest.GetGoalActor());
			if (NavGoal)
			{
				const FVector Offset = NavGoal->GetMoveGoalOffset(this);
				GoalLocation = FQuatRotationTranslationMatrix(MoveRequest.GetGoalActor()->GetActorQuat(), NavGoal->GetNavAgentLocation()).TransformPosition(Offset);
			}
			else
			{
				GoalLocation = MoveRequest.GetGoalActor()->GetActorLocation();
			}
		}

		FSharedConstNavQueryFilter NavFilter = UNavigationQueryFilter::GetQueryFilter(*NavData, this, MoveRequest.GetNavigationFilter());
		Query = FPathFindingQuery(*Controller, *NavData, Controller->GetNavAgentLocation(), GoalLocation, NavFilter);
		Query.SetAllowPartialPaths(MoveRequest.IsUsingPartialPaths());

		bResult = true;
	}
	else
	{
	}
	return bResult;
}

void AAIGod::Test(const FAIMoveRequest& MoveRequest)
{
	//FAIMoveRequest MoveReq(Goal);
	//MoveReq.SetUsePathfinding(bUsePathfinding);
	//MoveReq.SetAllowPartialPath(bAllowPartialPaths);
	//MoveReq.SetNavigationFilter(*FilterClass ? FilterClass : DefaultNavigationFilterClass);
	//MoveReq.SetAcceptanceRadius(AcceptanceRadius);
	//MoveReq.SetReachTestIncludesAgentRadius(bStopOnOverlap);
	//MoveReq.SetCanStrafe(bCanStrafe);

	FPathFindingQuery PFQuery;
	const bool bValidQuery = BuildPathfindingQuery(MoveRequest, PFQuery, nullptr);
	if (bValidQuery)
	{
		FNavPathSharedPtr Path;
		FindPathForMoveRequest(MoveRequest, PFQuery, Path);
	}
}

void AAIGod::SpawnBot()
{
	GetWorld()->SpawnActor<ABaseEnemy>(EnemyClass, FVector(FMath::RandRange(-1000, 1000), FMath::RandRange(-1000, 1000), FMath::RandRange(300, 500)), FRotator::ZeroRotator);
}

/// call every N seconds?
void AAIGod::DivideIntoGroups()
{

}

void AAIGod::UpdateTargetForEnemies(AActor* PreviousTarget)
{
	const auto EnemiesArr = EnemiesGroups.Find(PreviousTarget);
	if (EnemiesArr)
	{
		for (auto & Enemy : *EnemiesArr)
		{
			FindTargetFor(Enemy);
			EnemiesGroups.FindOrAdd(Enemy->GetTarget()).Add(Enemy);
		}
		EnemiesGroups.Remove(PreviousTarget);
	}
}

// Called every frame
void AAIGod::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveAllBoids(DeltaTime);
}

void AAIGod::RecalculateAllTargets()
{
	EnemiesGroups.Reset();
    for (auto & Enemy : Enemies)
    {
        FindTargetFor(Enemy);
		EnemiesGroups.FindOrAdd(Enemy->GetTarget()).Add(Enemy);
	}
}

void AAIGod::OnEnemiesDeath(AActor * DeadActor)
{
	ABaseEnemy * ActorAsEnemy = Cast<ABaseEnemy>(DeadActor);
	if (!ActorAsEnemy)
	{
		return;
	}
    Enemies.Remove(ActorAsEnemy);

	for (auto & EnemyGroup : EnemiesGroups)
	{
		EnemyGroup.Value.Remove(ActorAsEnemy);
	}

    DeadActor->SetLifeSpan(DeathTime);
//    DeadActor->Destroy();
}

void AAIGod::OnTargetDeath(AActor * DeadActor)
{
    Targets.Remove(Cast<ATower>(DeadActor));
    DeadActor->SetLifeSpan(DeathTime);
    
    RecalculateAllTargets();
    //    DeadActor->Destroy();
}

void AAIGod::OnObjectSpawned(AActor * SpawnedObject)
{
    if (auto ObjectAsEnemy = Cast<ABaseEnemy>(SpawnedObject))
    {
        ObjectAsEnemy->HealthComponent->OnDeath.AddUObject(this, &AAIGod::OnEnemiesDeath);
        Enemies.Add(ObjectAsEnemy);
		FindTargetFor(ObjectAsEnemy);
		EnemiesGroups.FindOrAdd(ObjectAsEnemy->GetTarget()).Add(ObjectAsEnemy);
    }
    else
    {
        if (auto ObjectAsBuilding = Cast<ATower>(SpawnedObject))
        {
            ObjectAsBuilding->HealthComponent->OnDeath.AddUObject(this, &AAIGod::OnTargetDeath);
            // TODO: Possibly requires cast/ cast check
            Targets.Add(ObjectAsBuilding);
			RecalculateAllTargets();
        }
    }
}

void AAIGod::FindTargetFor (ABaseEnemy * Object)
{
 //   if (Targets.Num())
    {
		AActor * BestTarget = GetWorld()->GetFirstPlayerController()->GetPawn();// Targets[0];
        float Dist = FVector::Distance(BestTarget->GetActorLocation(), Object->GetActorLocation());
        for (auto & Target : Targets)
        {
            const float CurDist = FVector::Distance(Target->GetActorLocation(), Object->GetActorLocation());
            if (Dist > CurDist)
            {
                Dist = CurDist;
                BestTarget = Target;
            }
        }
        Object->SetTarget(BestTarget);
    }
}

void AAIGod::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Super::EndPlay(EndPlayReason);
    Enemies.Empty();
    Targets.Empty();
}
PRAGMA_ENABLE_OPTIMIZATION_ACTUAL
