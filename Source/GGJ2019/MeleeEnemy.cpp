// Fill out your copyright notice in the Description page of Project Settings.

#include "MeleeEnemy.h"
#include "GameFramework/Controller.h"
#include "AIController.h"

bool AMeleeEnemy::IsAbleToAttack()
{
    return Target
    && FVector::Distance(GetActorLocation(), Target->GetActorLocation()) <= Range;
}


void AMeleeEnemy::Attack()
{
    if (IsAbleToAttack())
    {
		Super::Attack();
        Target->TakeDamage(DamageAmount, FDamageEvent(), GetInstigatorController(), this); 
    }

	//	AAIController * Controller;
//	Controller->MoveToLocation()

}

void AMeleeEnemy::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Attack();
}
