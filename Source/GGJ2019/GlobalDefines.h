//
//  GlobalDefines.h
//  GGJ2019
//
//  Created by Dmytro Lavoryk on 1/26/19.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#ifndef GlobalDefines_h
#define GlobalDefines_h

#include "CoreMinimal.h"


DECLARE_MULTICAST_DELEGATE_OneParam(FOnSpawned, AActor*);
/// @todo dlavoryk: Remove ALL static objects?s
class SpawnManager
{
	friend class AAIGod;

public:
private:

	FOnSpawned OnSpawned;

public:

	static SpawnManager & Get()
	{
		static SpawnManager Instance;
		return Instance;
	}

	PRAGMA_DISABLE_OPTIMIZATION_ACTUAL

	void ObjectSpawned(AActor * SpawnedObj)
	{
		OnSpawned.Broadcast(SpawnedObj);
	}

	PRAGMA_ENABLE_OPTIMIZATION_ACTUAL

};

#endif /* GlobalDefines_h */
