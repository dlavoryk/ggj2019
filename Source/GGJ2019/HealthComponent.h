// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

////////////////////////////////////////// Damage amount, Owner,   Damager  
DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnDamaged, float, AActor*, AActor*);
/////////////////////////////////    Owner
DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeath, AActor*);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GGJ2019_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UHealthComponent(float);

	FOnDeath OnDeath;
	FOnDamaged OnDamaged;

	float TakeDamage(float, AActor*);

	float GetMaxHealth() const;

	float GetCurrentHealth() const;

	void SetMaxHealth(float);

	void SetCurrentHealth(float);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	float maxHealth;
	float currentHealth;
		
};
