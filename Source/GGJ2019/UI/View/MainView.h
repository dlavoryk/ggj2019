// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TextBlock.h"
#include "ProgressBar.h"
#include "MainView.generated.h"

/**
 * 
 */
UCLASS()
class GGJ2019_API UMainView : public UUserWidget
{
	GENERATED_BODY()

public:

	void SetHealthBarValue(float Value);
	void SetHealthText(const FText & Value);
	//void SetAmmoText(const FString & Text);
	//void SetAmmoText(const FText & Text);
	
protected:
	UFUNCTION(BlueprintCallable) void Initwidgets(UProgressBar * HealthBar, /*UTextBlock * AmmoBlock,*/ UTextBlock * HealthBlock);

protected:
	UProgressBar * mHealthBar = nullptr;
	//UTextBlock   * mAmmo = nullptr;
	UTextBlock   * mHealth = nullptr;

};
